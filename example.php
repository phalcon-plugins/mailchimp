<?php

require_once('vendor/autoload.php');

use PhalconPlugins\MailChimp\MailChimp;

$data = [
    'sandbox' => false,
    'api_key' => 'your-api-key',
    'audience_id' => '849b006cf7',
    'user_full_name' => 'User Full NAME',
    'user_email' => 'user_email@test.com'
];

$mc = new MailChimp($data);

//OR

$mc = new MailChimp();

$mc->setApiKey('your-api-key');

$mc->setAudienceId('849b006cf7');

$mc->setUserEmail('user_email@test.com');

//subscribe user
//$mc->subscribe();

//unsubscribe user
$mc->unsubscribe();

$mc->sendRequest();

//get response body
$mc->getResponseBody();

//get response header
$mc->getResponseHeader();

/*@var $errors array */
$errors = $mc->getErrors();