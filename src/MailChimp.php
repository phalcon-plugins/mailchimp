<?php

namespace PhalconPlugins\MailChimp;

use PhalconPlugins\MailChimp\Constants\GlobalConstants;
use PhalconPlugins\MailChimp\Constants\MessagesConstants;
use PhalconPlugins\MailChimp\Response\ApiResponse;
use Phalcon\Http\Client\Request;
use Phalcon\Http\Client\Exception;
use Phalcon\Text;
use Phalcon\Config;

class MailChimp extends ApiResponse {

    /**
     * @var bool sandbox
     */
    protected $api_ver = '3.0';

    /**
     * @var bool sandbox
     */
    protected $sandbox;

    /**
     * @var string  Mail chimp API Key
     */
    protected $api_key;

    /**
     * @var string  Unique ID , Audience list ID
     */
    protected $audience_id;

    /**
     * @var string
     */
    protected $user_full_name;

    /**
     * @var string
     */
    protected $user_email;

    /**
     * @var bool
     */
    protected $user_status;

    /**
     * @var array  Error messages
     */
    protected $errors = [];

    /**
     * @var \Phalcon\Config
     */
    protected $response_body;

    /**
     * @var \Phalcon\Http\Client\Header
     */
    protected $response_header;

    /**
     * @param array $options []
     */
    public function __construct($options = []) {

        foreach ($options as $key => $value) {

            $method = 'set' . Text::camelize($key, '_');

            if (!method_exists(get_class(), $method)) {
                continue;
            }

            $this->{$method}($value);
        }
    }

    /**
     * @param string $api_key Mail chimp API Key
     */
    public function setApiKey($api_key) {
        $this->api_key = $api_key;
    }

    /**
     * @return string Mail chimp API Key
     */
    public function getApiKey() {
        return $this->api_key;
    }

    /**
     * @param string $audience_id Unique ID , Audience ID
     */
    public function setAudienceId($audience_id) {
        $this->audience_id = $audience_id;
    }

    /**
     * @return string Unique ID , Audience list ID
     */
    public function getAudienceId() {
        return $this->audience_id;
    }

    /**
     * @param bool $sandbox set Sandbox mode
     */
    public function setSandbox($sandbox = false) {

        $this->sandbox = $sandbox;
    }

    /**
     * @return bool check if in sandbox mode
     */
    public function isSandbox() {
        return $this->sandbox;
    }

    /**
     * @param string $user_full_name Full name
     */
    public function setUserFullName($user_full_name) {

        $this->user_full_name = $user_full_name;
    }

    /**
     * @return string
     */
    public function getUserFullName() {

        return $this->user_full_name;
    }

    /**
     * @param string $user_email Email address to subscribe or Un-subscribe
     */
    public function setUserEmail($user_email = '') {

        $this->user_email = $user_email;
    }

    /**
     * @return string
     */
    public function getUserEmail() {

        return $this->user_email;
    }

    /**
     * Subscribe user to Audience list
     */
    public function subscribe() {

        $this->user_status = GlobalConstants::STATUS_SUBSCRIBE;
    }

    /**
     * UnSubscribe user to Audience list
     */
    public function unsubscribe() {

        $this->user_status = GlobalConstants::STATUS_UNSUBSCRIBE;
    }

    /**
     * @return \Phalcon\Config
     */
    public function getResponseBody() {

        $data = json_decode($this->response_body, true);

        return is_array($data) ? new Config($data) : new Config([]);
    }

    /**
     * @return \Phalcon\Http\Client\Header Mailchimp response header
     */
    public function getResponseHeader() {

        return $this->response_header;
    }

    /**
     * @return string Unique ID , Audience list ID
     */
    public function getFirstError() {
        return !empty($this->errors[0]) ? $this->errors[0] : false;
    }

    /**
     * @return array List with error messages
     */
    public function getErrors() {
        return !empty($this->errors) ? $this->errors : false;
    }

    /**
     * @param string $message Error message
     */
    protected function setError($message = false) {

        if (empty($message)) {

            return false;
        }
        $this->errors[] = $message;
    }

    /**
     * Fix the name of the user to First name and Last name
     * 
     * @return array User first and last name
     */
    protected function splitName() {

        $name = explode(' ', $this->getUserFullName());

        $fname = [];

        if (empty(array_filter($name))) {

            return $fname;
        }

        $fname[] = $name[0];

        if (count($name) > 1) {

            unset($name[0]);

            $fname[] = implode(' ', $name);
        }

        return $fname;
    }

    /**
     * Send the request to Mail chimp API
     * 
     * @return bool true on success, false if errors
     */
    public function sendRequest() {

        $this->validate();

        if (!empty($this->getErrors())) {
            return false;
        }

        $mail_hash = \md5(\strtolower($this->getUserEmail()));

        $full_name = $this->splitName();

        $params = [
            'email_address' => $this->getUserEmail(),
            'status' => $this->user_status
        ];

        //add First and Last name
        !empty($full_name[0]) ?
                        $params['merge_fields']['FNAME'] = $full_name[0] :
                        false;

        !empty($full_name[1]) ?
                        $params['merge_fields']['LNAME'] = $full_name[1] :
                        false;

        $json = \json_encode($params);

        $dc = substr($this->getApiKey(), strpos($this->getApiKey(), '-') + 1);

        $provider = Request::getProvider();

        $provider->setBaseUri('https://' . $dc . '.api.mailchimp.com/' . $this->api_ver . '/lists/');

        $provider->header->set('Content-Type', 'application/json');

        $provider->header->set('Content-Length', strlen($json));

        $provider->header->set('Accept', 'application/json');

        $provider->setOption(CURLOPT_SSL_VERIFYPEER, false);

        $provider->setOption(CURLOPT_VERBOSE, true);

        $provider->setOption(CURLOPT_RETURNTRANSFER, true);

        $provider->setAuth('user', $this->getApiKey());

        $provider->setConnectTimeout(5);

        $provider->setTimeout(10);

        try {

            /* @var $request \Phalcon\Http\Client\Response */
            $request = $provider->put($this->getAudienceId() . '/members/' . $mail_hash, $json);
        } catch (Exception $exc) {

            $this->setError($exc->getMessage());

            return false;
        }

        /* @var $header \Phalcon\Http\Client\Header */
        $this->response_header = $request->header;

        $this->response_body = $request->body;

        switch ($this->getResponseHeader()->statusCode) {
            case 400:
            case 404:
                $this->setError($this->getResponseBody()->path('detail'));
                break;
            default:
                break;
        }

        $this->mapFields($this->getResponseBody());

        return true;
    }

    protected function validate() {

        $valid = [
            [
                'valid' => $this->getApiKey(),
                'error' => MessagesConstants::VALIDATE_ERROR_API_KEY
            ],
            [
                'valid' => $this->getUserEmail(),
                'error' => MessagesConstants::VALIDATE_USER_EMAIL
            ],
            [
                'valid' => $this->getAudienceId(),
                'error' => MessagesConstants::VALIDATE_AUDIENCE_ID
            ]
        ];

        foreach ($valid as $value) {
            if (empty($value['valid'])) {
                $this->setError($value['error']);
            }
        }
    }

}
