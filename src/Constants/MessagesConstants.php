<?php

namespace PhalconPlugins\MailChimp\Constants;

/**
 * class of MessagesConstants
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class MessagesConstants {

    /**
     * @var string
     */
    const VALIDATE_ERROR_API_KEY = 'You need to provide API key';

    /**
     * @var string
     */
    const VALIDATE_USER_NAME = 'You need to provide user Full name';

    /**
     * @var string
     */
    const VALIDATE_USER_EMAIL = 'You need to provide user Email';

    /**
     * @var string
     */
    const VALIDATE_AUDIENCE_ID = 'You need to provide Audience ID';

}
