<?php

namespace PhalconPlugins\MailChimp\Constants;

/**
 * class of GlobalConstants
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class GlobalConstants {

    /**
     * @var string
     */
    const STATUS_SUBSCRIBE = 'subscribed';

    /**
     * @var string
     */
    const STATUS_UNSUBSCRIBE = 'unsubscribed';

}
