<?php

namespace PhalconPlugins\MailChimp\Response;

use Phalcon\Config;

/**
 * class ApiResponse
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class ApiResponse {

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $email_address;

    /**
     * @var string
     */
    private $unique_email_id;

    /**
     * @var int
     */
    private $web_id;

    /**
     * @var string
     */
    private $email_type;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $unsubscribe_reason;

    /**
     * @var string
     */
    private $ip_signup;

    /**
     * @var string
     */
    private $timestamp_signup;

    /**
     * @var string
     */
    private $ip_opt;

    /**
     * @var string
     */
    private $timestamp_opt;

    /**
     * @var int
     */
    private $member_rating;

    /**
     * @var string
     */
    private $last_changed;

    /**
     * @var string
     */
    private $language;

    /**
     * @var string
     */
    private $vip;

    /**
     * @var string
     */
    private $email_client;

    /**
     * @var string
     */
    private $list_id;

    /**
     * @param \Phalcon\Config
     */
    protected function mapFields($params = \Phalcon\Config) {

        $data = !empty($params) ? $params : new Config([]);

        foreach (\get_class_vars(\get_class()) as $key => $value) {

            $this->{$key} = $data->path($key, $value);
        }
    }

    public function getId() {

        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmailAddress() {

        return $this->email_address;
    }

    /**
     * @return string
     */
    public function getUniqueEmailId() {

        return $this->unique_email_id;
    }

    /**
     * @return string
     */
    public function getWebId() {

        return $this->web_id;
    }

    /**
     * @return string
     */
    public function getEmailType() {

        return $this->email_type;
    }

    /**
     * @return string
     */
    public function getStatus() {

        return $this->status;
    }

    /**
     * @return string
     */
    public function getUnsubscribeReason() {

        return $this->unsubscribe_reason;
    }

    /**
     * @return string
     */
    public function getIpSignup() {

        return $this->ip_signup;
    }

    /**
     * @return string
     */
    public function getTimestampSignup() {

        return $this->timestamp_signup;
    }

    /**
     * @return string
     */
    public function getIpOpt() {

        return $this->ip_opt;
    }

    /**
     * @return string
     */
    public function getTimestampOpt() {

        return $this->timestamp_opt;
    }

    /**
     * @return string
     */
    public function getMemberRating() {

        return $this->member_rating;
    }

    /**
     * @return string
     */
    public function getLastChanged() {

        return $this->last_changed;
    }

    public function getLanguage() {

        return $this->language;
    }

    /**
     * @return string
     */
    public function getVip() {

        return $this->vip;
    }

    /**
     * @return string
     */
    public function getEmailClient() {

        return $this->email_client;
    }

    /**
     * @return string
     */
    public function getListId() {

        return $this->list_id;
    }

}
